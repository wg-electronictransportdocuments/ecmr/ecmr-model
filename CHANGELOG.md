# eCMR with origin information

# eCMR data model Versions:
0.3.2 (2024-12-20)
---------------------------

* Add `multiConsigneeShipment` with a Boolean field `isMultiConsigneeShipment` to the area TWO

0.3.1 (2024-12-10)
---------------------------

* Add `SealedDocument`

0.3.0 (2024-11-18)
---------------------------

* Add origin information (`originUrl`) as String to `EcmrModel` to support sharing eCMRs between different instances

0.2.18 (2024-11-20)
---------------------------

### Added
- Integration of the `SealedECMR` class into the `EcmrModelTest` to encapsulate ECMR data along with metadata, including the sealer's name and timestamp.
- Added a new part to the test method `printEcmrDataAsJson` in `EcmrModelTest` to demonstrate the serialization of a `SealedECMR` object to JSON, showcasing the complete ECMR data structure with associated metadata.
- Updating the documentation to reflect the changes made

0.2.17 (2024-09-24)
---------------------------

* Fix: Change data type of `...supplyChainConsignmentItemGrossVolume` from float to Integer

0.2.16 (2024-08-29)
---------------------------

* Fix: CI Pipeline fixes. No changes to the data model.

0.2.15 (2024-08-27)
---------------------------

* Fix: Set `...CompanyName` validation to default 255 characters

0.2.14 (2024-08-22)
---------------------------

* Fix: Remove validation of `Signature` fields entirely

0.2.13 (2024-08-05)
---------------------------

* Fix: Fields with type `Signature` should have `@Valid` annotation

0.2.12 (2024-07-10)
---------------------------

**Source-Code:**
* Remove cefact fields from `*CountryCode` types (they were remove in all other types in version `0.2.1`).

**Documentation:**
* Fix cefact numbers:
  * `*CountryCode` (ends with `UN01004546` instead of `UN01004547` (Country Name)).
  * `senderNameCompany` (formerly falsely used the same as `senderNamePerson`).
* Update JSON example to be in sync with previously changes.

0.2.11 (2024-07-05)
---------------------------

* Make `PayerType` enum fields upper case:
  * Sender -> SENDER
  * Consignee -> CONSIGNEE
* Declare field `ecmrStatus` of `EcmrModel` to use `JsonFormat.Shape.STRING`
* Update documentation

0.2.10 (2024-07-05)
---------------------------

* Add fields `createdAt`, `createdBy`, `editedAt`, `editedBy` to `EcmrModel`.

0.2.9 (2024-06-28)
---------------------------

* Add `EcmrStatus` as field of `EcmrModel`.
  * Possible values are: `NEW`, `LOADING`, `IN_TRANSPORT`, `ARRIVED_AT_DESTINATION`

0.2.8 (2024-06-25)
---------------------------

* Update documentation to reflect changes from 0.2.1 up to 0.2.7

0.2.7 (2024-06-25)
---------------------------

### Breaking Change:
* Renaming to be consistent with _Consignee_ and _Sender_:
  * SEVEN:
    * `CarrierInformation`
      * `successiveCarrierName` to `successiveCarrierNameCompany`
      * `successiveCarrierPersonName` to `successiveCarrierNamePerson`
      * `successiveCarrierStreetName` to `successiveCarrierStreet`

0.2.6 (2024-06-20)
---------------------------

### Breaking Change:
* Renaming to be consistent with _Consignee_ and _Sender_:
  * SIX:
    * `CarrierInformation`
        * `carrierName` to `carrierNameCompany`
        * `carrierPersonName` to `carrierNamePerson`
        * `carrierStreetName` to `carrierStreet`
  * SEVEN:
    * `SuccessiveCarriers` to `SuccessiveCarrierInformation`
* Refactoring:
  * SEVENTEEN:
    * Introduce class `CustomCharge` with attributes `value: Float`, `currency: String` and `payer: PayerType`.
    * Replace type of attributes of `ToBePaidBy` with new type `CustomCharge`.

### Fix:
* Area:
  * ELEVEN:
    * `NumberOfPackages.logisticsPackageItemQuantity` set value range to `0`-`9999` (instead of `0`-`4`).

0.2.5 (2024-06-14)
---------------------------

* Fix: Pattern of fields with type `java.time.Instant` changed from `yyyy-MM-dd'T'HH:mm:ss.SSSZ` to `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`
  * Note: The `'Z'` in single quotes means that the letter Z should appear literally in the date string, indicating Coordinated Universal Time (UTC).

0.2.4 (2024-06-13)
---------------------------

* Fix: remove falsely added annotation (mislead by JavaDoc in changed context)

0.2.3 (2024-06-13)
---------------------------

* Fix typo: rename package `eigthteen` to `eighteen`.

0.2.2 (2024-06-13)
---------------------------
Changes based on internal discussion

* Rename class `EcmrData` to `EcmrModel`.

0.2.1 (2024-06-13)
---------------------------
Changes based on internal discussion

* Remove nearly all classes in package `areas`  (except `twentysix.ReferenceIdentificationNumber`) containing only a single mutable attribute. Replaced by fields in the using classes with same validation constraints and information as before.

0.2.0 (2024-05-23)
---------------------------
Changes based on first working group <i>Electronic Transport Documents</i> feedback

* General Changes:
    * In the data model, all fields are optional --> only eCMR ID is mandatory
    * Background: Before signing, you must check whether certain fields are filled in or not. This is implemented in the
      eCMR service and not part of the data model.
* AREA Changes:
    * ONE
        - Replace SenderCountry with SenderCountryCode. Value has 2 chars. Always capitalised.
            - Add additional field: region (Datatype: String, Value Range 2-60 chars)
        - SenderPostcode: Spaces, letters and hyphen must be possible.
        - SenderStreet: Set value range to 2-255
        - Add new field: carrierContactInformation with
            - email (string 255)
            - phone (string, only numbers, leading 0)
    * TWO
        - Replace ConsigneeCountry with ConsigneeCountryCode. Value has 2 chars. Always capitalised.
            - Add additional field: region (Datatype: String, Value Range 2-60 chars)
        - ConsigneePostcode: Set value range to 2 - 17.
        - Add ConsigneeStreet
        - Add new field: consigneeContactInformation with
            - email (string 255)
            - phone (string, only numbers, leading 0)
    * THREE
        - Remove LogisticsEventActualOccurrence
        - Rename LogisticsTakingOverTheGoods to TakingOverTheGoodsPlace
        - Remove LogisticsTakingOverTheGoodsCountry
    * FOUR
        - Remove LogisticsCountry
        - Rename LogisticsLocationName to LogisticsLocationCity
        - LogisticsLocationOpeningHours set value range to 2-255 chars.
    * FIVE
        - TransportInstructionsDescription set value to 2-512 chars.
    * SIX
        - Replace CarrierCountry with CarrierCountryCode. Value has 2 chars. Always capitalised.
            - Add additional field: region (Datatype: String, Value Range 2-60 chars)
        - CarrierPostalcode: Spaces, letters and hyphen must be possible.
        - Set ConsigneeStreet value range to 2-255
        - Set LicensePLate value range to 2-15
        - Add new field: carrierContactInformation with
            - email (string 255)
            - phone (string, only numbers, leading 0)

    - SEVEN
        - Replace SuccessiveCarrierCountry with SuccessiveCarrierCountryCode . Value has 2 chars. Always capitalised.
            - Add additional field: region (Datatype: String, Value Range 2-60 chars)
        - Add new field: successiveCarrierContactInformation with
            - email (string 255)
            - phone (string, only numbers, leading 0)
    - ELEVEN
        - LogisticsPackageItemQuantity set value range to 0-4
    - FOURTEEN
        - SupplyChainConsignmentItemGrossWeight set value range to 1-99999
    - FIVETEEN
        - SupplyChainConsignmentItemGrossVolume change datatype to float
        - SupplyChainConsignmentItemGrossVolume set value range from 1 - 9999
    - SIXTEEN
        - Special Agreement set value range to 2-255
    - SEVENTEEN
        - CustomChargeCarriage set value range to 1-99999
        - CustomChargeSupplementary set value range to 1-99999
        - CustomChargeCustomsDuties set value range to 1-99999
        - CustomChargeOther set value range to 1-99999
    - NINETEEN
        - CustomCashOnDelivery set value range to 1-999999
    - TWENTYSIX
        - ReferenceIdentificationNumber set value range to 1-35

0.1.0-SNAPSHOT (2024-05-03)
---------------------------
Initial Release
