# eCMR Data Model

Exchange the content of this file with the most important information about your project. The following sections /
bullet points should help you better structure it.

## Disclaimer

The eCMR data model is currently under development and only a working status is available here
but not yet a usable version.

## Documentation

For more details, please refer to the `documentation` directory.

## License

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.

## Licenses of third-party dependencies

For information about licenses of third-party dependencies, please refer to the `README.md` file within the data-model
directory.

## Contact information

Team:

* Björn Krämer: <bjoern.kraemer@iml.fraunhofer.de>
* Jens Leveling:  <Jens.Leveling@iml.fraunhofer.de>
* Marcus Schröer: <Marcus.Schroeer@iml.fraunhofer.de>
* Niklas Burczyk: <niklas.burczyk@iml.fraunhofer.de>
* Till-Philipp Patron: <till-philipp.patron@iml.fraunhofer.de>
