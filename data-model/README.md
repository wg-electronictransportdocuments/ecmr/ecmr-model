# eCMR data model

the eCMR Data model is based on the
UN/CEFACT Electronic Road Consignment Note (eCMR)
standard.

More information can be found here:
https://service.unece.org/trade/uncefact/publication/Transport-Logistics/eCMR/HTML/001.htm

## Disclaimer

The eCMR data model is currently under development and only a working status is available here
but not yet a usable version.

## Versions

* Java: 17

## Usage

Created jars are available by the gitlab package registry and can be integrated in your project via Gradle or Maven
dependency.

Please select a version from the package registry and have a look at the details there.
