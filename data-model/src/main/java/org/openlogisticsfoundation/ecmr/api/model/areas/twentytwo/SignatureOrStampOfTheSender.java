/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.twentytwo;

import lombok.Data;
import org.openlogisticsfoundation.ecmr.api.model.signature.Signature;

/**
 * eCMR area 22.
 */
@Data
public class SignatureOrStampOfTheSender {

    /**
     * The value of the sender signature. Defined as CUSTOM_18.
     * <p>
     * The data type is Signature, representing the signature information.
     */
    private Signature senderSignature;
}
