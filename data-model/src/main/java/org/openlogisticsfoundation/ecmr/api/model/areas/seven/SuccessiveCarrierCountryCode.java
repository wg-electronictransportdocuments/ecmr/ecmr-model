/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.seven;

import lombok.Data;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

/**
 * This class defines the Successive Carrier Country. This is based on the TDED 3206 definition and is defined in
 * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004227_UN01004603_UN01004546.
 */
@Data
public class SuccessiveCarrierCountryCode {

    /**
     * The region or state of the successive carrier.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * This field can include spaces, letters, and hyphens.
     */
    @Size(min = 2, max = 60)
    private String region;

    /**
     * The country, expressed as text, of this successive carrier.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 2 characters.
     * <p>
     * The maximum length of 2 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 2)
    @Pattern(regexp = "^[A-Z]{2}$")
    private String value;
}
