/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.openlogisticsfoundation.ecmr.api.model.EcmrConsignment;
import org.openlogisticsfoundation.ecmr.api.model.areas.two.ConsigneeInformation;

public class MultiConsigneeShipmentValidator implements ConstraintValidator<ValidMultiConsigneeShipment, EcmrConsignment> {

    @Override
    public boolean isValid(EcmrConsignment ecmrConsignment, ConstraintValidatorContext context) {
        if (ecmrConsignment.getMultiConsigneeShipment() != null && ecmrConsignment.getMultiConsigneeShipment().getIsMultiConsigneeShipment() == Boolean.TRUE) {
            return areConsigneeInformationFieldsNull(ecmrConsignment.getConsigneeInformation());
        }
        return true;
    }

    boolean areConsigneeInformationFieldsNull(ConsigneeInformation consigneeInformation){
        if(consigneeInformation == null)
            return true;
        if(consigneeInformation.getConsigneeNameCompany() != null ||
            consigneeInformation.getConsigneeNamePerson() != null ||
            consigneeInformation.getConsigneePostcode() != null ||
            consigneeInformation.getConsigneeCity() != null ||
            (consigneeInformation.getConsigneeCountryCode() != null &&
                (consigneeInformation.getConsigneeCountryCode().getValue() != null ||
                    consigneeInformation.getConsigneeCountryCode().getRegion() != null)) ||
            consigneeInformation.getConsigneeStreet() != null ||
            (consigneeInformation.getConsigneeContactInformation() != null &&
                (consigneeInformation.getConsigneeContactInformation().getEmail() != null ||
                    consigneeInformation.getConsigneeContactInformation().getPhone() != null))){
            return false;
        }
        return true;
    }
}
