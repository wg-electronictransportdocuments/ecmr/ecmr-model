/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.six;

import lombok.Data;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;

/**
 * eCMR area 6.
 */
@Data
public class CarrierInformation {

    /**
     * This field defines the Carrier Name. This is based on the TDED 3036 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004214_UN01004598.
     * <p>
     * The data type is string, with a maximum length of 255 characters.
     * <p>
     * The maximum length of 255 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    private String carrierNameCompany;

    /**
     * This field defines the Carrier Person Name. This is based on the TDED 3412 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004214_UN01004602_UN01001642.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     */
    @Size(min = 2, max = 60)
    private String carrierNamePerson;

    /**
     * This field defines the Carrier Street Name. This is based on the TDED 3024 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004214_UN01004603_UN01004543.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 255 characters is Non-conformance to UN/CEFACT Standard, which allows a maximum length of
     * 256 characters.
     */
    @Size(min = 2, max = 255)
    private String carrierStreet;

    /**
     * This field defines the Carrier Postcode. This is based on the TDED 3251 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004214_UN01004603_UN01004535.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 17 characters.
     */
    @Size(min = 2, max = 17)
    private String carrierPostcode;

    /**
     * This field defines the Carrier City. This is based on the TDED 3164 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004214_UN01004603_UN01004544.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard, which allows a maximum length of 35
     * characters.
     */
    @Size(min = 2, max = 60)
    private String carrierCity;

    @Valid
    private CarrierCountryCode carrierCountryCode;

    /**
     * The license plate of the carrier. Defined as CUSTOM_03.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 15 characters.
     */
    @Size(min = 2, max = 15)
    private String carrierLicensePlate;

    @Valid
    private CarrierContactInformation carrierContactInformation;
}
