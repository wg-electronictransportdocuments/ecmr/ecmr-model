/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model;

import jakarta.validation.Valid;
import lombok.Data;
import org.openlogisticsfoundation.ecmr.api.model.areas.eight.CarriersReservationsAndObservationsOnTakingOverTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.eighteen.OtherUsefulParticulars;
import org.openlogisticsfoundation.ecmr.api.model.areas.five.SendersInstructions;
import org.openlogisticsfoundation.ecmr.api.model.areas.four.DeliveryOfTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.nine.DocumentsHandedToCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.nineteen.CashOnDelivery;
import org.openlogisticsfoundation.ecmr.api.model.areas.one.SenderInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.seven.SuccessiveCarrierInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.seventeen.ToBePaidBy;
import org.openlogisticsfoundation.ecmr.api.model.areas.six.CarrierInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.sixteen.SpecialAgreementsSenderCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.three.TakingOverTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentyfive.NonContractualPartReservedForTheCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentyfour.GoodsReceived;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentyone.Established;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentysix.ReferenceIdentificationNumber;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentythree.SignatureOrStampOfTheCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentytwo.SignatureOrStampOfTheSender;
import org.openlogisticsfoundation.ecmr.api.model.areas.two.ConsigneeInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.two.MultiConsigneeShipment;
import org.openlogisticsfoundation.ecmr.api.model.compositions.Item;
import org.openlogisticsfoundation.ecmr.api.model.validation.ValidMultiConsigneeShipment;

import java.util.List;

@Data
@ValidMultiConsigneeShipment
public class EcmrConsignment {

    // area 1

    @Valid
    private SenderInformation senderInformation;

    // area 2

    @Valid
    private MultiConsigneeShipment multiConsigneeShipment;

    @Valid
    private ConsigneeInformation consigneeInformation;

    // area 3

    @Valid
    private TakingOverTheGoods takingOverTheGoods;

    // area 4

    @Valid
    private DeliveryOfTheGoods deliveryOfTheGoods;

    // area 5

    @Valid
    private SendersInstructions sendersInstructions;

    // area 6

    @Valid
    private CarrierInformation carrierInformation;

    // area 7
    @Valid
    private SuccessiveCarrierInformation successiveCarrierInformation;

    // area 8

    @Valid
    private CarriersReservationsAndObservationsOnTakingOverTheGoods carriersReservationsAndObservationsOnTakingOverTheGoods;

    // area 9

    @Valid
    private DocumentsHandedToCarrier documentsHandedToCarrier;


    @Valid
    private List<Item> itemList;

    // area 16

    @Valid
    private SpecialAgreementsSenderCarrier specialAgreementsSenderCarrier;

    // area 17

    @Valid
    private ToBePaidBy toBePaidBy;

    // area 18

    @Valid
    private OtherUsefulParticulars otherUsefulParticulars;

    // area 19

    @Valid
    private CashOnDelivery cashOnDelivery;

    // area 21

    @Valid
    private Established established;

    // area 22

    @Valid
    private SignatureOrStampOfTheSender signatureOrStampOfTheSender;

    // area 23

    @Valid
    private SignatureOrStampOfTheCarrier signatureOrStampOfTheCarrier;

    // area 24

    @Valid
    private GoodsReceived goodsReceived;

    // area 25

    @Valid
    private NonContractualPartReservedForTheCarrier nonContractualPartReservedForTheCarrier;

    // area 26

    @Valid
    private ReferenceIdentificationNumber referenceIdentificationNumber;
}
