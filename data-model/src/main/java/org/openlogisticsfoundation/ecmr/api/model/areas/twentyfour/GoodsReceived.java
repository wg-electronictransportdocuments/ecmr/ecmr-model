/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.twentyfour;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.openlogisticsfoundation.ecmr.api.model.signature.Signature;

import jakarta.validation.constraints.Size;
import java.time.Instant;

/**
 * eCMR area 24.
 */
@Data
public class GoodsReceived {

    /**
     * The value of the confirmed logistics location name. Defined as CUSTOM_20.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     */
    @Size(min = 2, max = 60)
    private String confirmedLogisticsLocationName;

    /**
     * This field defines the Consignee Reservations Observations. This is based on the TDED 4440 definition and is
     * defined in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01012574_UN01002522.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 512 characters.
     */
    @Size(min = 2, max = 512)
    private String consigneeReservationsObservations;

    /**
     * The value of the consignee signature. Defined as CUSTOM_21.
     * <p>
     * The data type is Signature, representing the signature information.
     */
    private Signature consigneeSignature;

    /**
     * This field defines the Consignee Signature Date.  This is based on the TDED 2193 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004251_UN01004796.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant consigneeSignatureDate;

    /**
     * This field defines the Consignee Time of Arrival.  This is based on the TDED 2193 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004251_UN01004796.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant consigneeTimeOfArrival;

    /**
     * This field defines the Consignee Time of Departure. This is based on the TDED 2193 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004251_UN01004796.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant consigneeTimeOfDeparture;
}
