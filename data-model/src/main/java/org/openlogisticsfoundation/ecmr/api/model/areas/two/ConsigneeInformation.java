/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.two;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;

import lombok.Data;

/**
 * eCMR area 2.
 */
@Data
public class ConsigneeInformation {

    /**
     * This field represents the Consignee Name or Company. This is based on the TDED 3036 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004213_UN01004598.
     * <p>
     * The data type is string, with a maximum length of 255 characters.
     * <p>
     * The maximum length of 255 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    private String consigneeNameCompany;

    /**
     * This field represents the Consignee Person Name. This is based on the TDED 3412 definition and is defined in the
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004213_UN01004602_UN01001642.
     * <p>
     * It is a string with a minimum length of 2 characters and a maximum length of 60 characters.
     */
    @Size(min = 2, max = 60)
    private String consigneeNamePerson;

    /**
     * This field represents the Consignee Postcode. It is based on the TDED 3251 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004213_UN01004603_UN01004535.
     * <p>
     * It is a string with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 17 characters is conformance to UN/CEFACT Standard,
     * which allows a maximum length of 17 characters.
     */
    @Size(min = 2, max = 17)
    private String consigneePostcode;

    /**
     * This field represents the Consignee City. It is based on the TDED 3164 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004213_UN01004603_UN01004544.
     * <p>
     * It is a string with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 60)
    private String consigneeCity;

    @Valid
    private ConsigneeCountryCode consigneeCountryCode;

    /**
     * This field defines the Consignee Street. This is based on the TDED 3042 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004213_UN01004603_UN01004543.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 255 characters.
     * <p>
     * The maximum length of 255 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 256 characters.
     */
    @Size(min = 2, max = 255)
    private String consigneeStreet;

    @Valid
    private ConsigneeContactInformation consigneeContactInformation;
}
