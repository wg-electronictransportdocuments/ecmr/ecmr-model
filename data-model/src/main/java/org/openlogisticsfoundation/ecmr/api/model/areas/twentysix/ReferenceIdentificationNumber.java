/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.twentysix;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * eCMR area 26.
 */
@Data
public class ReferenceIdentificationNumber {

    /**
     * Custom code for the particular.
     */
    @JsonIgnore
    private final String customCode = "CUSTOM_01";

    /**
     * The value of the reference identification number.
     * <p>
     * The data type is string, with a minimum length of 1 character and a maximum length of 35 characters.
     */

    @Size(min = 1, max = 35)
    private String value;
}
