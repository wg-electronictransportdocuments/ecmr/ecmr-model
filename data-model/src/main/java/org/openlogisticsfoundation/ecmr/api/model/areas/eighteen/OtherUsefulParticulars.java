/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.eighteen;

import lombok.Data;

import jakarta.validation.constraints.Size;

/**
 * eCMR area 18.
 */
@Data
public class OtherUsefulParticulars {

    /**
     * The value of the particular. Defined as CUSTOM_15.
     * <p>
     * The value must be between 2 and 512 characters long.
     */
    @Size(min = 2, max = 512)
    private String customParticulars;
}
