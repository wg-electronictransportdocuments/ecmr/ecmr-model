/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.seventeen;

/**
 * Enum representing the types of payers in a transaction.
 */
public enum PayerType {
    SENDER,
    CONSIGNEE
}
