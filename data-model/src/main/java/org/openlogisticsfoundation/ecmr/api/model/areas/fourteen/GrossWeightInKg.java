/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.fourteen;

import lombok.Data;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * eCMR area 14.
 */
@Data
public class GrossWeightInKg {

    /**
     * This field defines the Supply Chain Consignment Item Gross Weight. This is based on the TDED 6292 definition and
     * is defined in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004258_UN01004121.
     * <p>
     * The data type is Integer, with a minimum value of 0 and a maximum value of 99,999.
     * <p>
     * The maximum length of 99,999 characters is Non-conformance to UN/CEFACT Standard, which allows a maximum length
     * of 14 characters.
     */
    @Min(1)
    @Max(99_999)
    // TODO: integer or double
    private Integer supplyChainConsignmentItemGrossWeight;
}
