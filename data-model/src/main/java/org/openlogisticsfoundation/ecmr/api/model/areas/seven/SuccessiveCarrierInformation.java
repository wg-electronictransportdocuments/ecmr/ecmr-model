/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.seven;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.openlogisticsfoundation.ecmr.api.model.signature.Signature;

import java.time.Instant;

/**
 * eCMR area 7.
 */
@Data
public class SuccessiveCarrierInformation {

    /**
     * This field defines the Successive Carrier. This is based on the TDED 3164 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004227_UN01004603_UN01004544.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 60)
    private String successiveCarrierCity;

    @Valid
    private SuccessiveCarrierCountryCode successiveCarrierCountryCode;

    /**
     * This field defines the Successive Carrier Name Company. This is based on the TDED 3036 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004227_UN01004598.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 255 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    private String successiveCarrierNameCompany;

    /**
     * This field defines the Successive Carrier Name Person. This is based on the TDED 3412 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004227_UN01004602_UN01001642.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 60)
    private String successiveCarrierNamePerson;

    /**
     * This field defines the Successive Carrier Postcode. This is based on the TDED 3251 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004227_UN01004603_UN01004535.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 17 characters.
     */
    @Size(min = 2, max = 17)
    private String successiveCarrierPostcode;

    /**
     * The signature of the successive carrier. Defined as CUSTOM_04.
     * <p>
     * The data type is Signature, with a minimum length of 2 characters and a maximum length of 1000 characters.
     */
    private Signature successiveCarrierSignature;

    /**
     * This field defines the Successive Carrier Signature Date. This is based on the TDED 2193 definition and is
     * defined in the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004254_UN01004796.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant successiveCarrierSignatureDate;

    /**
     * This field defines the Successive Carrier Street. This is based on the TDED 3042 definition and is defined
     * in the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004227_UN01004603_UN01004543.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 255 characters is conformance to UN/CEFACT Standard.
     */
    @Size(min = 2, max = 255)
    private String successiveCarrierStreet;

    @Valid
    private SuccessiveCarrierContactInformation successiveCarrierContactInformation;
}
