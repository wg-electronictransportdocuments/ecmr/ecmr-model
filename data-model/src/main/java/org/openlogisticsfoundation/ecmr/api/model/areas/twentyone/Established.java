/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.twentyone;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import jakarta.validation.constraints.Size;
import java.time.Instant;

/**
 * eCMR area 21.
 */
@Data
public class Established {

    /**
     * The value of the custom established date using the ISO 8601 Standard for date and time-related data with this
     * pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and UTC as timezone. Defined as CUSTOM_16.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant customEstablishedDate;

    /**
     * The value of the custom established in. Defined as CUSTOM_17.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 30 characters.
     */
    @Size(min = 2, max = 30)
    private String customEstablishedIn;
}
