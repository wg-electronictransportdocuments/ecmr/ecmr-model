/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.one;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * This class defines the Sender Country. This is based on the TDED 3206 definition and is defined in
 * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004212_UN01004603_UN01004546.
 */
@Data
public class SenderCountryCode {

    /**
     * The region or state of the sender.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * This field can include spaces, letters, and hyphens.
     */
    @Size(min = 2, max = 60)
    private String region;

    /**
     * The country code of the sender country.
     * CountryCode is based on ISO 3166-1 and is a two-letter code (e.g., DE for Germany, US for the USA).
     * The ISO 3166 Country Code is an internationally recognized standard
     * established by the International Organization for Standardization (ISO).
     * It is used for the unique identification of countries and dependent territories.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 2 characters.
     * <p>
     * The maximum length of 2 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 2)
    @Pattern(regexp = "^[A-Z]{2}$")
    private String value;
}
