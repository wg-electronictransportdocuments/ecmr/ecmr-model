/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.seventeen;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * Represents a custom charge with a value and currency, specifying who the payer is.
 * <p>
 * The value of the charge is constrained to be between 0 and 99999.
 * The maximum length of 99,999 characters is Non-conformance to UN/CEFACT Standard, which allows a maximum length
 * of 18 characters.
 * <p>
 * The currency is represented as a string and must be between 2 and 512 characters in length.
 * The payer is an enum of type PayerType, formatted as a string when serialized to JSON.
 */
@Data
public class CustomCharge {
    @Min(0)
    @Max(99_999)
    private Float value;
    @Size(min = 2, max = 512)
    private String currency;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private PayerType payer;
}
