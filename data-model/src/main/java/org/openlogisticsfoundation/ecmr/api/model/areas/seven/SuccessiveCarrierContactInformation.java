/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.seven;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * This class defines the Contact Information for the Successive Carrier Person.
 * <p>
 * Effective communication with successive carriers is facilitated through accurate contact details.
 */
@Data
public class SuccessiveCarrierContactInformation {

    /**
     * The email address of the successive carrier.
     * <p>
     * The data type is string, with a maximum length of 255 characters.
     */
    @Size(max = 255)
    private String email;

    /**
     * The phone number of the successive carrier, adhering to the ITU-T E.164 standard.
     * <p>
     * The data type is string, which should only contain numbers and can start with a leading '+'. The length should not exceed 15 digits.
     */
    @Size(max = 15)
    @Pattern(regexp = "\\+?[0-9]{1,15}")
    private String phone;
}
