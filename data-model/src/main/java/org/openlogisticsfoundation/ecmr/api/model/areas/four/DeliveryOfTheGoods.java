/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.four;

import jakarta.validation.constraints.Size;

import lombok.Data;

/**
 * eCMR area 4.
 */
@Data
public class DeliveryOfTheGoods {

    /**
     * This field defines the Logistics Location Name. This is based on the TDED 3224 definition and is defined in TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004237_UN01003681.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 256 characters.
     */
    @Size(min = 2, max = 60)
    private String logisticsLocationCity;

    /**
     * The opening hours of the logistics location. Defined as CUSTOM_02.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 255 characters.
     */
    @Size(min = 2, max = 255)
    private String logisticsLocationOpeningHours;
}
