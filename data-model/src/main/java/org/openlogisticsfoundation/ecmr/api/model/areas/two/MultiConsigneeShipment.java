/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.two;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * This class defines whether the shipment has multiple consignees.
 *
 */
@Data
public class MultiConsigneeShipment {

    /**
     * This field defines whether the shipment has multiple consignees.
     * <p>
     * The data type is boolean.
     */
    @NotNull
    private Boolean isMultiConsigneeShipment;
}
