/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.Instant;

@Data
public class SealedEcmr {

    @NotNull
    @Valid
    private Metadata metadata;

    @NotNull
    @Valid
    private EcmrModel ecmr;

    @Data
    public static class Metadata {
        /**
         * This field defines who sealed the document.
         */
        private String sealer;

        /**
         * This field defines the timestamp when the document was sealed.
         * <p>
         * Using the ISO 8601 Standard for date and time-related data with this pattern "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" and
         * UTC as timezone.
         * <p>
         * The data type is Instant, representing a point in time.
         */
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
        private Instant timestamp;
    }
}
