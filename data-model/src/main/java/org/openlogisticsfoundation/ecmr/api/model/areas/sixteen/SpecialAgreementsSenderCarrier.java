/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.sixteen;

import lombok.Data;

import jakarta.validation.constraints.Size;

/**
 * eCMR area 16.
 */
@Data
public class SpecialAgreementsSenderCarrier {

    /**
     * The value of the custom special agreement. Defined as CUSTOM_06.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 255 characters.
     */
    @Size(min = 2, max = 255)
    private String customSpecialAgreement;
}
