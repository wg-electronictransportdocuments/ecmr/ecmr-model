/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.signature;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.Instant;

/**
 * This class may be adapted in the course of the changeover to seals in accordance with the eIDAS standard
 */
@Data
public class Signature {

    private String type;

    private String userName;

    private String userCompany;

    private String userStreet;

    private String userPostCode;

    private String userCity;

    private String userCountry;

    /**
     * Timestamp of the signature using the ISO 8601 Standard for date and time-related data with
     * this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and UTC as timezone.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant timestamp;

    private String data;
}
