/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.eight;

import jakarta.validation.constraints.Size;

import lombok.Data;
import org.openlogisticsfoundation.ecmr.api.model.signature.Signature;

/**
 * eCMR area 8.
 */
@Data
public class CarriersReservationsAndObservationsOnTakingOverTheGoods {

    /**
     * This field defines the Carrier Reservations Observations. This is based on the TDED 4440 definition and is
     * defined in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01012581_UN010012572.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 512 characters.
     * <p>
     * The maximum length of 512 characters is conformance to UN/CEFACT Standard,
     * which allows a maximum length of 512 characters.
     */
    @Size(min = 2, max = 512)
    private String carrierReservationsObservations;

    /**
     * The signature of the sender reservations and observations. Defined as CUSTOM_05.
     * <p>
     * The data type is Signature.
     */
    private Signature senderReservationsObservationsSignature;
}
