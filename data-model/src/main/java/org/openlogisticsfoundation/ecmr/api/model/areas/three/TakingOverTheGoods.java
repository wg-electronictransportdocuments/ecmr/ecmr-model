/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.three;

import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.Instant;

/**
 * eCMR area 3.
 */
@Data
public class TakingOverTheGoods {

    /**
     * This field defines the Logistics Taking Over The Goods. This is based on the TDED 3224 definition and is defined
     * in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004235_UN01003681.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 256 characters.
     */
    @Size(min = 2, max = 60)
    private String takingOverTheGoodsPlace;

    /**
     * This field defines the Logistics Time Of Arrival Date. This is based on the TDED 2193 definition and is defined
     * in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004254_UN01004796.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant logisticsTimeOfArrivalDateTime;

    /**
     * This field defines the Logistics Time Of Departure Date. This is based on the TDED  2193 definition and is
     * defined in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004254_UN01004796.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant logisticsTimeOfDepartureDateTime;
}
