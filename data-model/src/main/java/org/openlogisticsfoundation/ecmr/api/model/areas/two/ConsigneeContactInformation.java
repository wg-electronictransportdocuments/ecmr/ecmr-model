/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.two;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * This class defines the Contact Information for the Consignee Person.
 * <p>
 * The email and phone fields are essential for communication purposes in logistics operations.
 */
@Data
public class ConsigneeContactInformation {

    /**
     * The email address of the consignee.
     * <p>
     * The data type is string, with a maximum length of 255 characters.
     */
    @Size(max = 255)
    private String email;

    /**
     * The phone number of the consignee, adhering to the ITU-T E.164 standard.
     * <p>
     * The data type is string, which should only contain numbers and can start with a leading '+'. The length should not exceed 15 digits.
     */
    @Size(max = 15)
    @Pattern(regexp = "\\+?[0-9]{1,15}")
    private String phone;
}
