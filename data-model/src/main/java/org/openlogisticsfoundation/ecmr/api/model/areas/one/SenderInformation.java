/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.one;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;

import lombok.Data;

/**
 * eCMR area 1.
 */
@Data
public class SenderInformation {

    /**
     * This field defines the Sender Name Company. This is based on the TDED 3036 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004212_UN01004598.
     * <p>
     * The data type is string, with a maximum length of 255 characters.
     * <p>
     * The maximum length of 255 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 255 characters.
     */
    private String senderNameCompany;

    /**
     * This field defines the Sender Person Name. This is based on the TDED 3412 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004212_UN01004602_UN01001642.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 60)
    private String senderNamePerson;

    /**
     * This field defines the Sender Street. This is based on the TDED 3042 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004212_UN01004603_UN01004543.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 255 characters.
     * <p>
     * The maximum length of 255 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 256 characters.
     */
    @Size(min = 2, max = 255)
    private String senderStreet;

    /**
     * This field defines the Sender Postcode. This is based on the TDED 3251 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004212_UN01004603_UN01004535.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 17 characters.
     */
    @Size(min = 2, max = 17)
    private String senderPostcode;

    /**
     * This field defines the Sender City. This is based on the TDED 3164 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004212_UN01004603_UN01004544.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 60 characters.
     * <p>
     * The maximum length of 60 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 60)
    private String senderCity;

    @Valid
    private SenderCountryCode senderCountryCode;

    @Valid
    private SenderContactInformation senderContactInformation;
}
