/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.eleven;

import lombok.Data;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * eCMR area 11.
 */
@Data
public class NumberOfPackages {

    /**
     * This field defines the Logistics Package Item Quantity. This is based on the TDED 7224 definition and is defined
     * in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004258_UN01004152_UN01003690.
     * <p>
     * The data type is Integer, with a minimum value of 0 and a maximum value of 9999.
     * <p>
     * The maximum length of 4 integers is Non-conformance to UN/CEFACT Standard, which allows a maximum length of 8
     * integers.
     */
    @Min(0)
    @Max(9999)
    private Integer logisticsPackageItemQuantity;
}
