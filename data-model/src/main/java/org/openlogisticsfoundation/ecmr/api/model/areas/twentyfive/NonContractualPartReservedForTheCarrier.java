/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.twentyfive;

import lombok.Data;

import jakarta.validation.constraints.Size;

/**
 * eCMR area 25.
 */
@Data
public class NonContractualPartReservedForTheCarrier {

    // TODO: this is set as 'custom_code' and not 'cefact' in 'ecmr.schema.json', why?
    /**
     * This field defines the Non Contractual Carrier Remarks.  This is based on the TDED 4002 definition and is defined
     * in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UNO1004258_UN01004149_UN01006029_UN01006020.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 512 characters.
     */
    @Size(min = 2, max = 512)
    private String nonContractualCarrierRemarks;
}
