/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.one;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * This class defines the Contact Information for the Sender Person.
 * <p>
 * The email and phone fields are essential for communication purposes in logistics operations.
 */
@Data
public class SenderContactInformation {

    /**
     * The email address of the sender.
     * <p>
     * The data type is string, with a maximum length of 255 characters.
     */
    @Size(max = 255)
    private String email;

    /**
     * The phone number, expressed as text, of the sender.
     * <p>
     * The data type is string, which should only contain numbers and can start with a leading '+'. The length should not exceed 15 digits.
     */
    @Size(max = 15)
    @Pattern(regexp = "\\+?[0-9]{1,15}")
    private String phone;
}
