/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.nineteen;

import lombok.Data;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * eCMR area 19.
 */
@Data
public class CashOnDelivery {

    /**
     * This field defines the Custom Cash On Delivery. This isn't defined in TDED Standard (UN/CEFACT) Code Hierarchy as
     * eCMR_UN01004159_UN01004189.
     * <p>
     * The data type is Integer, with a minimum value of 1 and a maximum value of 999,999.
     * <p>
     * The maximum length of 999,999 characters is Non-conformance to UN/CEFACT Standard, which allows a maximum length
     * of 18 characters.
     */
    @Min(1)
    @Max(999_999)
    private Integer customCashOnDelivery;
}
