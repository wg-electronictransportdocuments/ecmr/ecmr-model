/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.fifteen;

import lombok.Data;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * eCMR area 15.
 */
@Data
public class VolumeInM3 {

    /**
     * This field defines the Supply Chain Consignment Item Gross Volume. This is based on the TDED 6322 definition and
     * is defined in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004258_UN01004124.
     * <p>
     * The data type is float, with a minimum value of 1 and a maximum value of 9999.
     * <p>
     * The maximum length of 9,999 is Non-conformance to UN/CEFACT Standard, which allows a maximum length of numbers up
     * to 9,999 characters.
     */
    @Min(1)
    @Max(9_999)
    private Integer supplyChainConsignmentItemGrossVolume;
}
