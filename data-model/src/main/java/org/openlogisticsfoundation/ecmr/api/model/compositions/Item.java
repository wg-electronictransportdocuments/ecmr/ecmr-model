/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.compositions;

import jakarta.validation.Valid;
import lombok.Data;
import org.openlogisticsfoundation.ecmr.api.model.areas.eleven.NumberOfPackages;
import org.openlogisticsfoundation.ecmr.api.model.areas.fifteen.VolumeInM3;
import org.openlogisticsfoundation.ecmr.api.model.areas.fourteen.GrossWeightInKg;
import org.openlogisticsfoundation.ecmr.api.model.areas.ten.MarksAndNos;
import org.openlogisticsfoundation.ecmr.api.model.areas.thirteen.NatureOfTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.twelve.MethodOfPacking;

@Data
public class Item {

    // area 10

    @Valid
    private MarksAndNos marksAndNos;

    // area 11

    @Valid
    private NumberOfPackages numberOfPackages;

    // area 12

    @Valid
    private MethodOfPacking methodOfPacking;

    // area 13

    @Valid
    private NatureOfTheGoods natureOfTheGoods;

    // area 14

    @Valid
    private GrossWeightInKg grossWeightInKg;

    // area 15

    @Valid
    private VolumeInM3 volumeInM3;
}
