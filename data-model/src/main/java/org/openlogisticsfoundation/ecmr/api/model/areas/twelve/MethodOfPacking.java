/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.twelve;

import lombok.Data;

import jakarta.validation.constraints.Size;

/**
 * eCMR area 12.
 */
@Data
public class MethodOfPacking {

    /**
     * This field defines the Logistics Package Type. This is based on the TDED 7064 definition and is defined in TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004258_UN01004152_UN01003698.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 35 characters.
     */
    @Size(min = 2, max = 35)
    private String logisticsPackageType;
}
