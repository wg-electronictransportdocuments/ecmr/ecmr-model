/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.seventeen;

import jakarta.validation.Valid;
import lombok.Data;

/**
 * eCMR area 17.
 */
@Data
public class ToBePaidBy {

    /**
     * This field defines the Custom Charge Carriage. This is based on the TDED 5034 definition and is defined in the
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004285_UN01003730.
     */
    @Valid
    private CustomCharge customChargeCarriage;

    /**
     * This field defines the Custom Charge Supplementary. This is based on the TDED 5034 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004285_UN01003730.
     */
    @Valid
    private CustomCharge customChargeSupplementary;


    /**
     * This field defines the Custom Charge Customs Duties. This is based on the TDED 5034 definition and is defined in
     * the TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004285_UN01003730.
     */
    @Valid
    private CustomCharge customChargeCustomsDuties;


    /**
     * This field defines the Custom Charge Other. This is based on the TDED 5034 definition and is defined in the TDED
     * Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01004285_UN01003730.
     */
    @Valid
    private CustomCharge customChargeOther;
}
