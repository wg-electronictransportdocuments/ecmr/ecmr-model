/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.Instant;

@Data
public class EcmrModel {

    private String ecmrId;

    @NotNull
    @Valid
    private EcmrConsignment ecmrConsignment;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private EcmrStatus ecmrStatus;

    /**
     * This field defines the point in time this eCMR was created.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant createdAt;
    /**
     * This field defines the author of this eCMR.
     */
    private String createdBy;

    /**
     * This field defines the point in time this state of the eCMR was edited.
     * <p>
     * Using the ISO 8601 Standard for date and time-related data with this pattern \"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\" and
     * UTC as timezone.
     * <p>
     * The data type is Instant, representing a point in time.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant editedAt;
    /**
     * This field defines the editor of this state of the eCMR.
     */
    private String editedBy;

    private String originUrl;
}
