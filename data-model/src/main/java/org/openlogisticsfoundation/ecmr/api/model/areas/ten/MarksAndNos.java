/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.ten;

import lombok.Data;

import jakarta.validation.constraints.Size;

/**
 * eCMR area 10.
 */
@Data
public class MarksAndNos {

    /**
     * This field defines the Logistics Shipping Marks Marking. This is based on the TDED 7102 definition and is defined
     * in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01003705_UN01003742.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 512 characters.
     */
    @Size(min = 2, max = 512)
    private String logisticsShippingMarksMarking;

    /**
     * This field defines the Logistics Shipping Marks Custom Barcode. This is based on the TDED 7402 definition and is
     * defined in TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01003705_UN01003744_UN01003676.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 35 characters.
     */
    @Size(min = 2, max = 35)
    private String logisticsShippingMarksCustomBarcode;
}
