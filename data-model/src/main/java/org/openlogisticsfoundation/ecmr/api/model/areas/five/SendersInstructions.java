/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.five;

import jakarta.validation.constraints.Size;

import lombok.Data;

/**
 * eCMR area 5.
 */
@Data
public class SendersInstructions {

    /**
     * This field defines the Transport Instructions Description. This is based on the TDED 4400 definition and is defined in
     * TDED Standard (UN/CEFACT) Code Hierarchy as eCMR_UN01004159_UN01009003_UN01004813.
     * <p>
     * The data type is string, with a minimum length of 2 characters and a maximum length of 512 characters.
     * <p>
     * The maximum length of 512 characters is Non-conformance to UN/CEFACT Standard,
     * which allows a maximum length of 35 characters.
     */
    @Size(min = 2, max = 512)
    private String transportInstructionsDescription;
}
