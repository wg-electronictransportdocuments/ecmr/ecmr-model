/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model.areas.twentythree;

import lombok.Data;
import org.openlogisticsfoundation.ecmr.api.model.signature.Signature;

/**
 * eCMR area 23.
 */
@Data
public class SignatureOrStampOfTheCarrier {

    /**
     * The value of the carrier signature. Defined as CUSTOM_19.
     * <p>
     * The data type is Signature, representing the signature information.
     */
    private Signature carrierSignature;
}
