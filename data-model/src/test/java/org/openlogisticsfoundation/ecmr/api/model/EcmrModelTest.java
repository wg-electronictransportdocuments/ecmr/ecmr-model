/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.openlogisticsfoundation.ecmr.api.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.ecmr.api.model.areas.eight.CarriersReservationsAndObservationsOnTakingOverTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.eighteen.OtherUsefulParticulars;
import org.openlogisticsfoundation.ecmr.api.model.areas.eleven.NumberOfPackages;
import org.openlogisticsfoundation.ecmr.api.model.areas.fifteen.VolumeInM3;
import org.openlogisticsfoundation.ecmr.api.model.areas.five.SendersInstructions;
import org.openlogisticsfoundation.ecmr.api.model.areas.four.DeliveryOfTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.fourteen.GrossWeightInKg;
import org.openlogisticsfoundation.ecmr.api.model.areas.nine.DocumentsHandedToCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.nineteen.CashOnDelivery;
import org.openlogisticsfoundation.ecmr.api.model.areas.one.SenderContactInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.one.SenderCountryCode;
import org.openlogisticsfoundation.ecmr.api.model.areas.one.SenderInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.seven.SuccessiveCarrierContactInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.seven.SuccessiveCarrierCountryCode;
import org.openlogisticsfoundation.ecmr.api.model.areas.seven.SuccessiveCarrierInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.seventeen.CustomCharge;
import org.openlogisticsfoundation.ecmr.api.model.areas.seventeen.PayerType;
import org.openlogisticsfoundation.ecmr.api.model.areas.seventeen.ToBePaidBy;
import org.openlogisticsfoundation.ecmr.api.model.areas.six.CarrierContactInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.six.CarrierCountryCode;
import org.openlogisticsfoundation.ecmr.api.model.areas.six.CarrierInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.sixteen.SpecialAgreementsSenderCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.ten.MarksAndNos;
import org.openlogisticsfoundation.ecmr.api.model.areas.thirteen.NatureOfTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.three.TakingOverTheGoods;
import org.openlogisticsfoundation.ecmr.api.model.areas.twelve.MethodOfPacking;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentyfive.NonContractualPartReservedForTheCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentyfour.GoodsReceived;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentyone.Established;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentysix.ReferenceIdentificationNumber;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentythree.SignatureOrStampOfTheCarrier;
import org.openlogisticsfoundation.ecmr.api.model.areas.twentytwo.SignatureOrStampOfTheSender;
import org.openlogisticsfoundation.ecmr.api.model.areas.two.ConsigneeContactInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.two.ConsigneeCountryCode;
import org.openlogisticsfoundation.ecmr.api.model.areas.two.ConsigneeInformation;
import org.openlogisticsfoundation.ecmr.api.model.areas.two.MultiConsigneeShipment;
import org.openlogisticsfoundation.ecmr.api.model.compositions.Item;
import org.openlogisticsfoundation.ecmr.api.model.signature.Signature;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * This class is used to test the functionality of the EcmrModel class.
 * It ensures that the classes created can be used correctly and provides an example of the resulting JSON document.
 */
class EcmrModelTest {

    @SneakyThrows
    @Test
    @DisplayName("print ecmr data as json")
    void printEcmrDataAsJson() {
        // arrange
        final Instant now = Instant.parse("2024-07-10T09:56:58.638Z");
        final Signature signature = new Signature();
        signature.setType("signature-type");
        signature.setUserName("userName");
        signature.setUserCompany("userCompany");
        signature.setUserStreet("userStreet");
        signature.setUserPostCode("userPostCode");
        signature.setUserCity("userCity");
        signature.setUserCountry("userCountry");
        signature.setTimestamp(now);
        signature.setData("");

        // area 1
        final SenderCountryCode senderCountryCode = new SenderCountryCode();
        senderCountryCode.setValue("EX");
        senderCountryCode.setRegion("Example region");
        final SenderContactInformation senderContactInformation = new SenderContactInformation();
        senderContactInformation.setEmail("sender@email.info");
        senderContactInformation.setPhone("+49000000000000");

        final SenderInformation senderInformation = new SenderInformation();
        senderInformation.setSenderNameCompany("sender name company");
        senderInformation.setSenderNamePerson("sender name person");
        senderInformation.setSenderStreet("sender street");
        senderInformation.setSenderPostcode("sender postcode");
        senderInformation.setSenderCity("sender city");
        senderInformation.setSenderCountryCode(senderCountryCode);
        senderInformation.setSenderContactInformation(senderContactInformation);

        // area 2
        final MultiConsigneeShipment multiConsigneeShipment = new MultiConsigneeShipment();
        multiConsigneeShipment.setIsMultiConsigneeShipment(false);

        final ConsigneeCountryCode consigneeCountryCode = new ConsigneeCountryCode();
        consigneeCountryCode.setValue("EX");
        consigneeCountryCode.setRegion("Example region");
        final ConsigneeContactInformation consigneeContactInformation = new ConsigneeContactInformation();
        consigneeContactInformation.setEmail("consignee@email.info");
        consigneeContactInformation.setPhone("+49000000000000");

        final ConsigneeInformation consigneeInformation = new ConsigneeInformation();
        consigneeInformation.setConsigneeNameCompany("consignee name");
        consigneeInformation.setConsigneeNamePerson("consignee name person");
        consigneeInformation.setConsigneePostcode("consignee postcode");
        consigneeInformation.setConsigneeCity("consignee city");
        consigneeInformation.setConsigneeCountryCode(consigneeCountryCode);
        consigneeInformation.setConsigneeStreet("consignee street");

        // area 3
        final TakingOverTheGoods takingOverTheGoods = new TakingOverTheGoods();
        takingOverTheGoods.setTakingOverTheGoodsPlace("logistics taking over the goods");

        takingOverTheGoods.setLogisticsTimeOfArrivalDateTime(now);
        takingOverTheGoods.setLogisticsTimeOfDepartureDateTime(now);

        // area 4
        final DeliveryOfTheGoods deliveryOfTheGoods = new DeliveryOfTheGoods();
        deliveryOfTheGoods.setLogisticsLocationCity("logistics location name");
        deliveryOfTheGoods.setLogisticsLocationOpeningHours("logistics location opening hours");

        // area 5
        final SendersInstructions sendersInstructions = new SendersInstructions();
        sendersInstructions.setTransportInstructionsDescription("Transport Instructions Description");

        // area 6
        final CarrierCountryCode carrierCountryCode = new CarrierCountryCode();
        carrierCountryCode.setValue("EX");
        carrierCountryCode.setRegion("Example region");
        final CarrierContactInformation carrierContactInformation = new CarrierContactInformation();
        carrierContactInformation.setEmail("carrier@email.info");
        carrierContactInformation.setPhone("+49000000000000");

        final CarrierInformation carrierInformation = new CarrierInformation();
        carrierInformation.setCarrierNameCompany("Carrier Name");
        carrierInformation.setCarrierNamePerson("Carrier Person (Name)");
        carrierInformation.setCarrierPostcode("Carrier Postcode");
        carrierInformation.setCarrierStreet("Carrier Street");
        carrierInformation.setCarrierCity("Carrier City");
        carrierInformation.setCarrierLicensePlate("Carrier License Plate");
        carrierInformation.setCarrierCountryCode(carrierCountryCode);
        carrierInformation.setCarrierContactInformation(carrierContactInformation);

        // area 7
        final SuccessiveCarrierCountryCode successiveCarrierCountryCode = new SuccessiveCarrierCountryCode();
        successiveCarrierCountryCode.setValue("EX");
        successiveCarrierCountryCode.setRegion("Example region");
        final SuccessiveCarrierContactInformation successiveCarrierContactInformation = new SuccessiveCarrierContactInformation();
        successiveCarrierContactInformation.setEmail("successiveCarrier@email.info");
        successiveCarrierContactInformation.setPhone("+49000000000000");

        final SuccessiveCarrierInformation successiveCarrierInformation = new SuccessiveCarrierInformation();
        successiveCarrierInformation.setSuccessiveCarrierCity("successive carrier city");
        successiveCarrierInformation.setSuccessiveCarrierCountryCode(successiveCarrierCountryCode);
        successiveCarrierInformation.setSuccessiveCarrierNameCompany("successive carrier name company");
        successiveCarrierInformation.setSuccessiveCarrierNamePerson("successive carrier name person");
        successiveCarrierInformation.setSuccessiveCarrierPostcode("successive carrier postcode");
        successiveCarrierInformation.setSuccessiveCarrierSignature(signature);
        successiveCarrierInformation.setSuccessiveCarrierSignatureDate(now);
        successiveCarrierInformation.setSuccessiveCarrierStreet("successive carrier street");
        successiveCarrierInformation.setSuccessiveCarrierContactInformation(successiveCarrierContactInformation);

        // area 8
        final CarriersReservationsAndObservationsOnTakingOverTheGoods carriersReservationsAndObservationsOnTakingOverTheGoods = new CarriersReservationsAndObservationsOnTakingOverTheGoods();
        carriersReservationsAndObservationsOnTakingOverTheGoods.setCarrierReservationsObservations("Carrier Reservation Observation");
        carriersReservationsAndObservationsOnTakingOverTheGoods.setSenderReservationsObservationsSignature(signature);

        // area 9
        final DocumentsHandedToCarrier documentsHandedToCarrier = new DocumentsHandedToCarrier();
        documentsHandedToCarrier.setDocumentsRemarks("documents remarks");

        // area 10
        final MarksAndNos marksAndNos = new MarksAndNos();
        marksAndNos.setLogisticsShippingMarksMarking("logistics shipping marks marking");
        marksAndNos.setLogisticsShippingMarksCustomBarcode("logistics shipping marks custom barcode");

        // area 11
        final NumberOfPackages numberOfPackages = new NumberOfPackages();
        numberOfPackages.setLogisticsPackageItemQuantity(100_000_000);

        // area 12
        final MethodOfPacking methodOfPacking = new MethodOfPacking();
        methodOfPacking.setLogisticsPackageType("logistics package type");

        // area 13
        final NatureOfTheGoods natureOfTheGoods = new NatureOfTheGoods();
        natureOfTheGoods.setTransportCargoIdentification("transport cargo identification");

        // area 14
        final GrossWeightInKg grossWeightInKg = new GrossWeightInKg();
        grossWeightInKg.setSupplyChainConsignmentItemGrossWeight(100_000_000);

        // area 15
        final VolumeInM3 volumeInM3 = new VolumeInM3();
        volumeInM3.setSupplyChainConsignmentItemGrossVolume(100_000_000);

        // area 16
        final SpecialAgreementsSenderCarrier specialAgreementsSenderCarrier = new SpecialAgreementsSenderCarrier();
        specialAgreementsSenderCarrier.setCustomSpecialAgreement("custom special agreement");

        // area 17
        final ToBePaidBy toBePaidBy = new ToBePaidBy();
        CustomCharge customChargeCarriage = new CustomCharge();
        customChargeCarriage.setCurrency("EUR");
        customChargeCarriage.setPayer(PayerType.CONSIGNEE);
        customChargeCarriage.setValue(1f);
        toBePaidBy.setCustomChargeCarriage(customChargeCarriage);
        CustomCharge customChargeSupplementary = new CustomCharge();
        customChargeSupplementary.setCurrency("EUR");
        customChargeSupplementary.setPayer(PayerType.SENDER);
        customChargeSupplementary.setValue(5f);
        toBePaidBy.setCustomChargeSupplementary(customChargeSupplementary);
        CustomCharge customChargeDuties = new CustomCharge();
        customChargeDuties.setCurrency("EUR");
        customChargeDuties.setPayer(PayerType.CONSIGNEE);
        customChargeDuties.setValue(100f);
        toBePaidBy.setCustomChargeCustomsDuties(customChargeDuties);
        CustomCharge customChargeOther = new CustomCharge();
        customChargeOther.setCurrency("EUR");
        customChargeOther.setPayer(PayerType.CONSIGNEE);
        customChargeOther.setValue(1f);
        toBePaidBy.setCustomChargeOther(customChargeOther);

        // area 18
        final OtherUsefulParticulars otherUsefulParticulars = new OtherUsefulParticulars();
        otherUsefulParticulars.setCustomParticulars("custom particular");

        // area 19
        final CashOnDelivery cashOnDelivery = new CashOnDelivery();
        cashOnDelivery.setCustomCashOnDelivery(100_000_000);

        // area 21
        final Established established = new Established();
        established.setCustomEstablishedIn("custom established in");
        established.setCustomEstablishedDate(now);

        // area 22
        final SignatureOrStampOfTheSender signatureOrStampOfTheSender = new SignatureOrStampOfTheSender();
        signatureOrStampOfTheSender.setSenderSignature(signature);

        // area 23
        final SignatureOrStampOfTheCarrier signatureOrStampOfTheCarrier = new SignatureOrStampOfTheCarrier();
        signatureOrStampOfTheCarrier.setCarrierSignature(signature);

        // area 24
        final GoodsReceived goodsReceived = new GoodsReceived();
        goodsReceived.setConsigneeReservationsObservations("confirmed logistics location name");
        goodsReceived.setConfirmedLogisticsLocationName("consignee reservations observations");
        goodsReceived.setConsigneeSignature(signature);
        goodsReceived.setConsigneeTimeOfDeparture(now);
        goodsReceived.setConsigneeSignatureDate(now);
        goodsReceived.setConsigneeTimeOfArrival(now);

        // area 25
        final NonContractualPartReservedForTheCarrier nonContractualPartReservedForTheCarrier = new NonContractualPartReservedForTheCarrier();
        nonContractualPartReservedForTheCarrier.setNonContractualCarrierRemarks("non contractual carrier remarks");

        // area 26
        final ReferenceIdentificationNumber referenceIdentificationNumber = new ReferenceIdentificationNumber();
        referenceIdentificationNumber.setValue("reference identification number");

        // formular
        final EcmrConsignment ecmrConsignment = new EcmrConsignment();
        // area 1
        ecmrConsignment.setSenderInformation(senderInformation);
        // area 2
        ecmrConsignment.setMultiConsigneeShipment(multiConsigneeShipment);
        ecmrConsignment.setConsigneeInformation(consigneeInformation);
        // area 3
        ecmrConsignment.setTakingOverTheGoods(takingOverTheGoods);
        // area 4
        ecmrConsignment.setDeliveryOfTheGoods(deliveryOfTheGoods);
        // area 5
        ecmrConsignment.setSendersInstructions(sendersInstructions);
        // area 6
        ecmrConsignment.setCarrierInformation(carrierInformation);
        // area 7
        ecmrConsignment.setSuccessiveCarrierInformation(successiveCarrierInformation);
        // area 8
        ecmrConsignment.setCarriersReservationsAndObservationsOnTakingOverTheGoods(carriersReservationsAndObservationsOnTakingOverTheGoods);
        // area 9
        ecmrConsignment.setDocumentsHandedToCarrier(documentsHandedToCarrier);

        final Item firstItem = new Item();
        // area 10
        firstItem.setMarksAndNos(marksAndNos);
        // area 11
        firstItem.setNumberOfPackages(numberOfPackages);
        // area 12
        firstItem.setMethodOfPacking(methodOfPacking);
        // area 13
        firstItem.setNatureOfTheGoods(natureOfTheGoods);
        // area 14
        firstItem.setGrossWeightInKg(grossWeightInKg);
        // area 15
        firstItem.setVolumeInM3(volumeInM3);
        ecmrConsignment.setItemList(List.of(firstItem));

        // area 16
        ecmrConsignment.setSpecialAgreementsSenderCarrier(specialAgreementsSenderCarrier);
        // area 17
        ecmrConsignment.setToBePaidBy(toBePaidBy);
        // area 18
        ecmrConsignment.setOtherUsefulParticulars(otherUsefulParticulars);
        // area 19
        ecmrConsignment.setCashOnDelivery(cashOnDelivery);
        // area 20

        // area 21
        ecmrConsignment.setEstablished(established);
        // area 22
        ecmrConsignment.setSignatureOrStampOfTheSender(signatureOrStampOfTheSender);
        // area 23
        ecmrConsignment.setSignatureOrStampOfTheCarrier(signatureOrStampOfTheCarrier);
        // area 24
        ecmrConsignment.setGoodsReceived(goodsReceived);
        // area 25
        ecmrConsignment.setNonContractualPartReservedForTheCarrier(nonContractualPartReservedForTheCarrier);
        // area 26
        ecmrConsignment.setReferenceIdentificationNumber(referenceIdentificationNumber);

        final EcmrModel ecmrModel = new EcmrModel();
        ecmrModel.setEcmrConsignment(ecmrConsignment);
        ecmrModel.setEcmrStatus(EcmrStatus.NEW);
        ecmrModel.setCreatedAt(now);
        ecmrModel.setCreatedBy("Creator");
        ecmrModel.setEditedAt(now.plus(2, ChronoUnit.HOURS));
        ecmrModel.setEditedBy("Editor");

        // metadata setup
        final SealedEcmr.Metadata metadata = new SealedEcmr.Metadata();
        metadata.setSealer("Max Mustermann");
        metadata.setTimestamp(now);

        // create sealed ecmr
        final SealedEcmr sealedEcmr = new SealedEcmr();
        sealedEcmr.setMetadata(metadata);
        sealedEcmr.setEcmr(ecmrModel);

        final ObjectWriter objectWriter = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .writerWithDefaultPrettyPrinter();

        // act
        final String json = objectWriter.writeValueAsString(sealedEcmr);
        System.out.println(json);
    }
}
